program mp2
  implicit none;

  character(len=32)   :: output
  double precision    :: epsilon
  integer             :: problem = -1
  integer, parameter  :: output_stream = 13

  write(*, *) 'CS131MP2 - RKF45'
  call assign_filename(output)
  open(unit=output_stream, file=output)
  
  do
    call assign_problem(problem)

    if (problem == 0) then
      exit
    else if (problem > 3 .or. problem < 0) then
      write(*, *) 'There is no such problem. Please choose only from (1-3).'
    else
      call assign_epsilon(epsilon)
      call solve(problem, epsilon, output_stream)
    end if
  end do
  
  close(output_stream)

  contains
    subroutine RKF45(xi, Yi, xout, Yout, epsilon, h, n, m, p)
      double precision, parameter                         :: a2 = 1.0/4.0, a3 = 3.0/8.0, a4 = 12.0/13.0, a6 = 1.0/2.0
      double precision                                    :: xi, xout, epsilon, h, alpha
      double precision, dimension(:)                      :: Yi, Yout
      double precision, dimension(:), allocatable         :: K1, K2, K3, K4, K5, K6, Y1, Y2, R
      integer                                             :: n, m, p

      allocate(K1(n))
      allocate(K2(n))
      allocate(K3(n))
      allocate(K4(n))
      allocate(K5(n))
      allocate(K6(n))
      allocate(Y1(n))
      allocate(Y2(n))
      allocate(R(n))

      K1 = 0
      K2 = 0
      K3 = 0
      K4 = 0
      K5 = 0
      K6 = 0
      Y1 = 0
      Y2 = 0
      R = 0

      do while(xi < xout)
        if (xi + h > xout) then
          h = xout - xi
        end if

        m = m + 1

        K1 = f(p, 1, n, xi, Yi)
        K2 = f(p, 2, n, xi + (a2 * h), modify(Yi, 2, n, h, K1, K2, K3, K4, K5, K6))
        K3 = f(p, 3, n, xi + (a3 * h), modify(Yi, 3, n, h, K1, K2, K3, K4, K5, K6))
        K4 = f(p, 4, n, xi + (a4 * h), modify(Yi, 4, n, h, K1, K2, K3, K4, K5, K6))
        K5 = f(p, 5, n, xi + h       , modify(Yi, 5, n, h, K1, K2, K3, K4, K5, K6))
        K6 = f(p, 6, n, xi + (a6 * h), modify(Yi, 6, n, h, K1, K2, K3, K4, K5, K6))

        Y1 = get_Y1(Yi, h, n, K1, K3, K4, K5)

        Y2 = get_Y2(Yi, h, n, K1, K3, K4, K5, K6)
        R = abs((Y2 - Y1))/h
        
        alpha = minval(get_R(R, n, epsilon))

        if(less_epsilon(R, n, epsilon) == 1) then
          xi = xi + h
          Yi = Y1
        end if

        if(alpha <= 0.1) then
          h = h * 0.1
        else if(alpha >= 4.0) then
          h = h * 4.0
        else
          h = alpha * h
        end if

        !write(*, *) 'm:', m
        !write(*, *) 'xi:', xi
        !write(*, *) 'xout:', xout
        !write(*, *) 'h:', h
        !write(*, *) 'epsilon:', epsilon
        !call print_values(1, K1, K2, K3, K4, K5, K6, Y1, Y2, R, alpha)
      end do

      Yout = Y1
    end subroutine RKF45
    
    subroutine solve(problem, epsilon, stream)
      integer          :: problem, stream
      double precision :: epsilon

      if (problem == 1) then
        call problem_1(epsilon, stream)
      else if (problem == 2) then
        call problem_2(epsilon, stream)
      else
        call problem_3(epsilon, stream)
      end if
    end subroutine solve

    subroutine problem_1(epsilon, stream)
      double precision                            :: xi, xout, epsilon, h
      integer                                     :: n, m, problem, stream
      double precision, dimension(:), allocatable :: Yi, Yout

      !epsilon = 1.0D-6
      h = epsilon**(1.0/4.0)
      n = 4
      problem = 1

      allocate(Yi(n))
      allocate(Yout(n))

      Yi(1) = 0
      Yi(2) = 0
      Yi(3) = 440
      Yi(4) = 0
      Yout = Yi

      xi = 0.0
      xout = 1.0

      call print_header(stream, problem, epsilon)
      do while(xout <= 61)
        call print_row(stream, xi, m, Yout, h)
        m = 0
        call RKF45(xi, Yi, xout, Yout, epsilon, h, n, m, problem)
        xi = xout
        xout = xout + 1.0
      end do

      write(stream, *)

      deallocate(Yi)
      deallocate(Yout)
    end subroutine problem_1

    subroutine problem_2(epsilon, stream)
      double precision                            :: xi, xout, epsilon, h, interval, max
      integer                                     :: n, m, problem, stream
      double precision, dimension(:), allocatable :: Yi, Yout

      !epsilon = 1.0D-7
      h = epsilon**(1.0/4.0)
      n = 4
      problem = 2

      allocate(Yi(n))
      allocate(Yout(n))

      Yi(1) = 1.2
      Yi(2) = 0.0
      Yi(3) = 0.0
      Yi(4) = -1.04935751
      Yout = Yi

      xi = 0.0
      xout = 6.19216933/10.0
      interval = xout
      max = 6.19216933*2.0

      call print_header(stream, problem, epsilon)
      do while(xout <= max+interval)
        call print_row(stream, xi, m, Yout, h)
        m = 0
        call RKF45(xi, Yi, xout, Yout, epsilon, h, n, m, problem)
        xi = xout
        xout = xout + interval
      end do
      
      write(stream, *)
      
      deallocate(Yi)
      deallocate(Yout)
    end subroutine problem_2

    subroutine problem_3(epsilon, stream)
      double precision                            :: xi, xout, epsilon, h, interval, max, alpha, e, pi
      integer                                     :: n, m, problem, stream
      double precision, dimension(:), allocatable :: Yi, Yout

      !epsilon = 1.0D-7
      h = epsilon**(1.0/4.0)
      n = 4
      m = 0
      problem = 3

      allocate(Yi(n))
      allocate(Yout(n))

      pi = 4.0*atan(1.0)
      alpha = pi/4.0
      e = 0.25
      
      Yi(1) = 1 - e
      Yi(2) = 0.0
      Yi(3) = 0.0
      Yi(4) = alpha*sqrt((1+e)/(1-e))

      xi = 0.0
      xout = ((2.0 * pi)/alpha)/10.0
      interval = xout
      max = ((4.0 * pi)/alpha)

      call print_header(stream, problem, epsilon)
      do while(xout <= max+(2*interval))
        call print_row(stream, xi, m, Yout, h)
        m = 0
        call RKF45(xi, Yi, xout, Yout, epsilon, h, n, m, problem)
        xi = xout
        xout = xout + interval
      end do
      
      write(stream, *)
      
      deallocate(Yi)
      deallocate(Yout)
    end subroutine problem_3

    function modify(Y, i, n, h, K1, K2, K3, K4, K5, K6)
      implicit none
      integer                                             :: i, j, n
      double precision                                    :: h
      double precision, dimension(:)                      :: Y, K1, K2, K3, K4, K5, K6
      double precision, dimension(:), allocatable         :: modify

      allocate(modify(n))
      do j = 1, n
        if (i == 2) then
          modify(j) = Y(j) + ((h * K1(j))/4.0)
        else if (i == 3) then
          modify(j) = Y(j) + ((h * ((3.0 * K1(j)) + (9.0 * K2(j))))/32.0)
        else if (i == 4) then
          modify(j) = Y(j) + ((h * ((1932.0 * K1(j)) - (7200.0 * K2(j)) + (7296.0 * K3(j))))/2197.0)
        else if (i == 5) then
          modify(j) = Y(j) + ((439.0 * h * K1(j))/216.0) - (8.0 * h * K2(j)) + ((3680.0 * h * K3(j))/513.0)
          modify(j) = modify(j) - ((845.0 * h * K4(j))/4104.0)
        else if (i == 6) then
          modify(j) = Y(j) - ((8.0 * h * K1(j))/27.0) + (2.0 * h * K2(j)) - ((3544.0 * h * K3(j))/2565.0)
          modify(j) = modify(j) + ((1859.0 * h * K4(j))/4104.0) - ((11.0 * h * K5(j))/40.0)
        end if
      end do
    end function modify

    function get_Y1(Y, h, n, K1, K3, K4, K5)
      implicit none
      integer                                             :: i, n
      double precision                                    :: h
      double precision, dimension(:), allocatable         :: get_Y1
      double precision, dimension(:)                      :: Y, K1, K3, K4, K5

      allocate(get_Y1(n))
      do i=1, n
        get_Y1(i) = Y(i) + (h * (((25.0 * K1(i))/216.0) + ((1408 * K3(i))/2565.0) + ((2197.0 * K4(i))/4104.0) - (K5(i)/5.0)))
      end do
    end function get_Y1

    function get_Y2(Y, h, n, K1, K3, K4, K5, K6)
      implicit none
      integer                                             :: i, n
      double precision                                    :: h, t1, t2, t3, t4, t5
      double precision, dimension(:), allocatable         :: get_Y2
      double precision, dimension(:)                      :: Y, K1, K3, K4, K5, K6

      allocate(get_Y2(n))
      do i=1, n
        t1 = (16.0 * K1(i))/135.0
        t2 = (6656.0 * K3(i))/12825.0
        t3 = (28561.0 * K4(i))/56430.0
        t4 = (9.0 * K5(i))/50.0
        t5 = (2.0 * K6(i))/55.0
        get_Y2(i) = Y(i) + (h * (t1 + t2 + t3 - t4 + t5))
      end do
    end function get_Y2

    function get_R(R, n, epsilon)
      double precision, dimension(:), allocatable :: get_R
      double precision, dimension(:) :: R
      integer :: i, n
      double precision :: epsilon

      allocate(get_R(n))
      do i=1, n
        if(R(i) == 0) then
          get_R(i) = 4
        else
          get_R(i) = 0.84 * ((epsilon/R(i))**(1.0/4.0))
        end if
      end do
    end function get_R

    function less_epsilon(R, n, epsilon)
      double precision, dimension(:) :: R
      double precision :: epsilon
      integer :: less_epsilon, i, n

      do i=1, n
        if(R(i) >= epsilon) then
          less_epsilon = 0
          return
        end if
      end do

      less_epsilon = 1
    end function less_epsilon

    function f(problem, i, n, x, Y)
      implicit none
      integer                                     :: problem, i, n
      double precision                            :: x
      double precision, dimension(:), allocatable :: f
      double precision, dimension(:)              :: Y

      allocate(f(n))

      if (problem == 1) then
        f(1) = f_1_1(x, Y)
        f(2) = f_1_2(x, Y)
        f(3) = f_1_3(x, Y)
        f(4) = f_1_4(x, Y)
      else if (problem == 2) then
        f(1) = f_2_1(x, Y)
        f(2) = f_2_2(x, Y)
        f(3) = f_2_3(x, Y)
        f(4) = f_2_4(x, Y)
      else
        f(1) = f_3_1(x, Y)
        f(2) = f_3_2(x, Y)
        f(3) = f_3_3(x, Y)
        f(4) = f_3_4(x, Y)
      end if
    end function f

    function f_1_1(x, Y)
      double precision                            :: f_1_1, x
      double precision, dimension(:)              :: Y
      
      f_1_1 = Y(3)
    end function f_1_1

    function f_1_2(x, Y)
      double precision                            :: f_1_2, x
      double precision, dimension(:)              :: Y
      
      f_1_2 = Y(4)
    end function f_1_2

    function f_1_3(x, Y)
      double precision                            :: f_1_3, x, v, u, w, g
      double precision, dimension(:)              :: Y
      g = 32.17
      v = 160.0
      u = Y(3)
      w = Y(4)
      f_1_3 = -(g/v**2)*(sqrt(u**2 + w**2))*u
    end function f_1_3

    function f_1_4(x, Y)
      double precision                            :: f_1_4, x, v, u, w, g
      double precision, dimension(:)              :: Y
      g = 32.17
      v = 160.0
      u = Y(3)
      w = Y(4)
      f_1_4 = g - ((g/v**2)*(sqrt(u**2 + w**2))*w)
    end function f_1_4

    function f_2_1(x, Y)
      double precision                            :: f_2_1, x
      double precision, dimension(:)              :: Y

      f_2_1 = Y(3)
    end function f_2_1

    function f_2_2(x, Y)
      double precision                            :: f_2_2, x
      double precision, dimension(:)              :: Y
      
      f_2_2 = Y(4)
    end function f_2_2

    function f_2_3(t, Yx)
      double precision                            :: f_2_3, t, x, y, w, alpha, beta, rho1, rho2
      double precision, dimension(:)              :: Yx
      x = Yx(1)
      y = Yx(2)
      w = Yx(4)

      alpha = 1.0/82.45
      beta = 1.0 - alpha
      rho1 = ((x + alpha)**2.0 + (y**2.0))**(3.0/2.0)
      rho2 = ((x - beta)**2.0 + (y**2.0))**(3.0/2.0)
      f_2_3 = (2.0*w) + x - ((beta*(x + alpha))/rho1) - ((alpha*(x-beta))/rho2)
    end function f_2_3

    function f_2_4(t, Yx)
      double precision                            :: f_2_4, t, x, y, u, w, alpha, beta, rho1, rho2
      double precision, dimension(:)              :: Yx
      x = Yx(1)
      y = Yx(2)
      u = Yx(3)
      w = Yx(4)
      alpha = 1.0/82.45
      beta = 1.0 - alpha
      rho1 = ((x + alpha)**2.0 + (y**2.0))**(3.0/2.0)
      rho2 = ((x - beta)**2.0 + (y**2.0))**(3.0/2.0)
      f_2_4 = (-2.0*u) + y - ((beta*y)/rho1) - ((alpha*y)/rho2)
    end function f_2_4

    function f_3_1(x, Y)
      double precision                            :: f_3_1, x
      double precision, dimension(:)              :: Y

      f_3_1 = Y(3)
    end function f_3_1

    function f_3_2(x, Y)
      double precision                            :: f_3_2, x
      double precision, dimension(:)              :: Y
      
      f_3_2 = Y(4)
    end function f_3_2

    function f_3_3(t, Yx)
      double precision                            :: f_3_3, t, x, y, alpha, R, e, pi
      double precision, dimension(:)              :: Yx

      x = Yx(1)
      y = Yx(2)
      pi = 4.0*atan(1.0)
      alpha = pi/4.0
      e = 0.25
      R = (x**2.0 + y**2.0)**(3.0/2.0)
      f_3_3 = -((alpha**2)*x)/R
    end function f_3_3

    function f_3_4(t, Yx)
      double precision                            :: f_3_4, t, x, y, alpha, R, e, pi
      double precision, dimension(:)              :: Yx

      x = Yx(1)
      y = Yx(2)
      pi = 4.0*atan(1.0)
      alpha = pi/4.0
      e = 0.25
      R = (x**2.0 + y**2.0)**(3.0/2.0)
      f_3_4 = -((alpha**2)*y)/R
    end function f_3_4

    subroutine print_header(stream, problem, epsilon)
      double precision :: epsilon
      integer          :: problem, stream

      write(stream, *) 'Problem ', problem
      write(stream, *) 'Epsilon:', epsilon
      write(stream, '(T12,A)', advance = 'no') 't'
      write(stream, '(T26,A)', advance = 'no') 'm'
      write(stream, '(T12,A)', advance = 'no') 'x'
      write(stream, '(T28,A)', advance = 'no') 'y'
      write(stream, '(T23,A)', advance = 'no') 'dx/dt'
      write(stream, '(T19,A)', advance = 'no') 'dy/dt'
      write(stream, '(T26,A)') 'h'
    end subroutine print_header

    subroutine print_row(stream, x, m, Y, h)
      double precision, dimension(:) :: Y
      double precision :: x, h
      integer :: stream, m
      write(stream, *)  x, m, Y, h
    end subroutine print_row

    subroutine print_values(stream, K1, K2, K3, K4, K5, K6, Y1, Y2, R, alpha)
      double precision, dimension(:) :: K1, K2, K3, K4, K5, K6
      double precision, dimension(:) :: Y1, Y2, R
      double precision :: alpha
      integer :: stream

      write(*, *) 'K1:', K1
      write(*, *) 'K2:', K2
      write(*, *) 'K3:', K3
      write(*, *) 'K4:', K4
      write(*, *) 'K5:', K5
      write(*, *) 'K6:', K6
      write(*, *) 'Y1:', Y1
      write(*, *) 'Y2:', Y2
      write(*, *) 'R:' , R
      write(*, *) 'alpha:' , alpha
      write(*, *) '======='
    end subroutine print_values

    subroutine assign_filename(filename)
      character(len=32) :: filename
      write(*, *) 'Enter the filename of external file:'
      read(*, *) filename
    end subroutine assign_filename

    subroutine assign_problem(problem)
      integer          :: problem
      write(*, *) 'Enter problem to be solved: (Enter zero to exit)'
      read(*, *) problem
    end subroutine assign_problem

    subroutine assign_epsilon(epsilon)
      double precision :: epsilon
      write(*, *) 'Enter error tolerance:'
      read(*, *) epsilon
    end subroutine assign_epsilon
end program mp2
